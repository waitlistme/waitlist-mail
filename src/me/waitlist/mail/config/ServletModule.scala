package me.waitlist.mail
package config

import scala.collection.JavaConversions.mapAsJavaMap
import com.sun.jersey.api.core.PackagesResourceConfig
import com.sun.jersey.api.core.ResourceConfig
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer
import me.waitlist.mail.http.MailServlet
import me.waitlist.mail.http.TestServlet

/**
 * Servlet configuration for establishing filters and servlets (e.g., RESTful endpoints).
 *
 * @author dsumera
 */
class ServletModule extends me.waitlist.core.config.ServletModule {

  override def configureServlets {

    super.configureServlets
    
    // TODO - test remove
    //serve("/test") `with` classOf[TestServlet]
    
    // crons
    
    // email crons
    serve("/cron/mail/*") `with` classOf[MailServlet]
    
    // tasks
    
    // configure mailing resources
    serve("/*") `with` (classOf[GuiceContainer],

      // configuration parameters
      Map(
        // where to find root resources
        PackagesResourceConfig.PROPERTY_PACKAGES -> "me.waitlist.mail.rs",

        // disable WADL
        ResourceConfig.FEATURE_DISABLE_WADL -> "true", // pojo mapping - MANDATORY
        
        //JSONConfiguration.FEATURE_POJO_MAPPING -> true,
        "com.sun.jersey.api.json.POJOMappingFeature" -> "true"
        ))
  }
}