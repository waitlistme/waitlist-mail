package me.waitlist.mail
package config

import com.google.inject.name.Names
import com.google.inject.Provides
import javax.inject.Singleton
import freemarker.template.Configuration
import java.io.File
import freemarker.template.TemplateExceptionHandler
import javax.mail.Address
import javax.mail.internet.InternetAddress
import freemarker.template.DefaultObjectWrapperBuilder
import freemarker.template.Version

/**
 * Bindings for <code>mail</code> module.
 *
 * @author dsumera
 */
class ServerModule extends me.waitlist.core.config.ServerModule {

  override def configure {

    // invoke all standard configurations
    super.configure

    // named-parameter bindings
    bind(classOf[Address]).annotatedWith(Names.named("email_default"))
      .toInstance(new InternetAddress("noreply@noshlist.com", "Waitlist Me"))

    bind(classOf[Address]).annotatedWith(Names.named("email_developer"))
      .toInstance(new InternetAddress("dennis@waitlist.me", "Dennis"))
      
    bind(classOf[Address]).annotatedWith(Names.named("email_support"))
      .toInstance(new InternetAddress("support@noshlist.com", "Waitlist Me"))

    // premium cost
    bind(classOf[String]) annotatedWith(Names.named("price_premium")) toInstance "19.99"
    
    // pro cost
    bind(classOf[String]) annotatedWith(Names.named("price_pro")) toInstance "39.99"
    
    // beta reminder time in days ahead
    bind(classOf[Int]) annotatedWith(Names.named("days_beta")) toInstance 4
    
    // tips reminder time in days past
    bind(classOf[Int]) annotatedWith(Names.named("days_tips")) toInstance 7

    // main url
    bind(classOf[String]) annotatedWith Names.named("url") toInstance "https://www.waitlist.me"
  }

  /**
   * <code>Configuration</code> template for freemarker.
   */
  @Provides
  @Singleton
  def provideConfiguration = {
    val cfg = new Configuration(Configuration.VERSION_2_3_24)

    // Specify the source where the template files come from. Here I set a
    // plain directory for it, but non-file-system sources are possible too:
    cfg.setDirectoryForTemplateLoading(new File("WEB-INF/templates"))

    // Set the preferred charset template files are stored in. UTF-8 is
    // a good choice in most applications:
    cfg.setDefaultEncoding("UTF-8")

    // setting the object wrapper
    cfg.setObjectWrapper((new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_24)).build)

    // Sets how errors will appear.
    // During web page *development* TemplateExceptionHandler.HTML_DEBUG_HANDLER is better.
    cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER)

    // Don't log exceptions inside FreeMarker that it will thrown at you anyway:
    cfg.setLogTemplateExceptions(false)

    // return configuration
    cfg
  }
}