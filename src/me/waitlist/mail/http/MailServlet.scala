package me.waitlist.mail
package http

import java.util.Calendar
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton
import me.waitlist.core.actor.MailActor
import me.waitlist.core.actor.MailActor.SendBeta
import me.waitlist.core.actor.MailActor.SendTips
import me.waitlist.core.dao.PlaceDao
import me.waitlist.core.dao.UserDao
import me.waitlist.core.dao.crit.BetweenCriterion
import me.waitlist.core.dao.crit.EQCriterion
import me.waitlist.core.http.DispatchServlet
import me.waitlist.core.traits.Dateable

@Singleton
class MailServlet extends DispatchServlet with Dateable {

  // days until first billing scheduled
  @Inject @Named("days_beta") private[this] var daysBeta: Int = _

  // days after account was added
  @Inject @Named("days_tips") private[this] var daysTips: Int = _

  // task actors
  @Inject private[this] var mailActor: MailActor = _

  // daos
  @Inject private[this] var placeDao: PlaceDao = _
  @Inject private[this] var userDao: UserDao = _

  // crons always mapped against GET
  override def dispatch = {
    // send a beta trial ending reminder
    case "beta" =>
      // retrieve all places where billing date is four days from now
      val midnight = getMidnight
      midnight.add(Calendar.DATE, daysBeta)
      val begin = midnight.getTime

      // until end of same day
      midnight.add(Calendar.DATE, 1)
      val end = midnight.getTime

      // find all beta places with next billing date within window
      placeDao.find(BetweenCriterion("next_billing_date", begin, end), EQCriterion("is_beta", true))( /* no loads */ )._1
        // that are not appdirect or clover customers
        .filterNot { p => Option(p.appdirect_uid).isDefined || Option(p.clover_id).isDefined }
        // send each a beta email
        .foreach { mailActor ! SendBeta(_) }

    // send weekly stats
    //case "stats" => mailActor ! SendStats
      
    // send a user tips reminder
    case "tips" =>
      // time offset for tips
      val midnight = getMidnight
      midnight.add(Calendar.DATE, -daysTips)
      val begin = midnight.getTime

      // until the end of same day
      midnight.add(Calendar.DATE, 1)
      val end = midnight.getTime

      // send users who fit within elapsed time a tips email
      userDao.find(BetweenCriterion("added_date", begin, end))( /* no loads */ )._1
        .foreach { mailActor ! SendTips(_) }
  }
}