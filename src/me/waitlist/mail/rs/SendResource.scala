package me.waitlist.mail
package rs

import javax.ws.rs.core.MediaType.{ APPLICATION_JSON => json, MULTIPART_FORM_DATA => data, TEXT_HTML => html, TEXT_PLAIN => text }
import freemarker.template.SimpleHash
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton
import javax.mail.Address
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMultipart
import javax.ws.rs.DefaultValue
import javax.ws.rs.FormParam
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.Response
import me.waitlist.core.dao.UserDao
import me.waitlist.core.db.User
import me.waitlist.core.gae.txn.Transact
import me.waitlist.core.rs.exception.NotFoundException
import me.waitlist.mail.rs.traits.Mailable
import com.googlecode.objectify.TxnType
import me.waitlist.core.dao._
import me.waitlist.core.dao.crit.EQCriterion
import java.util.logging.Logger
import com.googlecode.objectify.Key
import me.waitlist.core.db.Place
import me.waitlist.core.traits.Dateable
import com.google.appengine.api.memcache.AsyncMemcacheService
import me.waitlist.core.actor.MailActor
import me.waitlist.core.actor.MailActor.SendError

/**
 * Mail resource for direct email options.
 *
 * @author dsumera
 */
@Singleton
class SendResource extends Mailable
  with Dateable {

  // daos
  @Inject private[this] var placeDao: PlaceDao = _
  @Inject private[this] var userDao: UserDao = _
  @Inject private[this] var userPlaceDao: UserPlaceDao = _

  // actors
  @Inject private[this] var mail: MailActor = _
  
  // services
  @Inject private[this] var memcache: AsyncMemcacheService = _
  
  // addresses
  @Inject @Named("email_default") private[this] var default: Address = _
  @Inject @Named("email_developer") private[this] var developer: Address = _
  @Inject @Named("email_support") private[this] var support: Address = _

  // pricing
  @Inject @Named("price_premium") private[this] var premium: String = _
  @Inject @Named("price_pro") private[this] var pro: String = _
  
  // url
  @Inject @Named("url") private[this] var url: String = _
  
  @Inject var log: Logger = _

  @Path("beta")
  @POST
  @Produces(Array(json))
  def beta(@DefaultValue("-1")@FormParam("place") p: Long) = placeDao.find(p) match {
    case None =>
      val exc = new NotFoundException(s"place:$p not found")
      mail ! SendError(exc)
      throw exc
      
    case Some(place) => userPlaceDao.find(EQCriterion("place", Key.create(classOf[Place], p)))(classOf[User])._1 match {
      case Nil =>
        val exc = new NotFoundException(s"place:$p admin could not be found")
        mail ! SendError(exc)
        throw exc
        
      case up :: _ =>
        // model
        val root = new SimpleHash
        val user = up.user.get
        val tier = if (place.is_pro) "Pro" else "Premium"
        root.put("first", user.first_name)
        root.put("tier", tier)
        root.put("amount", if (place.is_pro) pro else premium)
        root.put("billing", place.next_billing_date)
        root.put("URL", s"$url/")
        root.put("images_url", s"$url/static/img/")

        // bind model to template as email body
        val body = new MimeMultipart("alternative")
        body addBodyPart bindText(root, "eml_beta_txt.ftl")
        body addBodyPart bindHtml(root, "eml_beta_html.ftl")

        // send email
        sendMail(support)(user.email.getEmail)(s"Your Waitlist Me $tier trial is expiring soon", body)

        // send OK response
        Response.ok.build
    }
  }

  /**
   * Send Clover password email.
   */
  @Path("clover")
  @POST
  @Produces(Array(json))
  def clover(
    @DefaultValue("-1")@FormParam("user") u: Long,
    @DefaultValue("")@FormParam("password") password: String) = userDao.find(u) match {
    case None =>
      val exc = new NotFoundException(s"user:$u not found")
      mail ! SendError(exc)
      throw exc
      
    case Some(user) =>
      val root = new SimpleHash
      root.put("user", user.first_name)
      root.put("password", password)
      root.put("URL", s"$url/")
      root.put("images_url", s"$url/static/img/")

      val body = new MimeMultipart("alternative")
      body addBodyPart bindText(root, "eml_clover_txt.ftl")
      body addBodyPart bindHtml(root, "eml_clover_html.ftl")

      sendMail(support)(new InternetAddress(user.email.getEmail, user.fullName))(
        "Your Waitlist Me Password", body)

      Response.ok.build
  }
    
  /**
   * Send contact email.
   */
  @Path("contact")
  @POST
  @Produces(Array(json))
  def contact(
    @DefaultValue("-1")@FormParam("first_name") first: String,
    @DefaultValue("")@FormParam("last_name") last: String,
    @DefaultValue("")@FormParam("business_name") business: String,
    @DefaultValue("0")@FormParam("num_accounts") accounts: Int,
    @DefaultValue("")@FormParam("email") email: String,
    @DefaultValue("")@FormParam("phone") phone: String,
    @DefaultValue("")@FormParam("comments") comments: String) = {
    val root = new SimpleHash
    root.put("first_name", first)
    root.put("last_name", last)
    root.put("business_name", business)
    root.put("num_accounts", accounts)
    root.put("email", email)
    root.put("phone", phone)
    root.put("comments", comments)
    root.put("URL", s"$url/")
    root.put("images_url", s"$url/static/img/")

    val body = new MimeMultipart("alternative")
    body addBodyPart bindText(root, "eml_contact_txt.ftl")
    body addBodyPart bindHtml(root, "eml_contact_html.ftl")

    sendMail(default)(support)(s"Waitlist Me contact request from $first $last", body, Option(email))

    Response.ok.build
  }

  /**
   * Send an error message to developer email.
   */
  @Path("error")
  @POST
  @Produces(Array(json))
  def error(@DefaultValue("")@FormParam("error") e: String) = {
    val root = new SimpleHash
    root.put("error", e)
    root.put("URL", s"$url/")
    root.put("images_url", s"$url/static/img/")

    val body = new MimeMultipart("alternative")
    body addBodyPart bindText(root, "eml_error_txt.ftl")
    body addBodyPart bindHtml(root, "eml_error_html.ftl")

    sendMail(default)(developer)(s"Waitlist Me error", body)

    Response.ok.build
  }
    
  /**
   * Send limit reminder email to the specified <code>Place</code> <code>User</code>s.
   */
  @Path("limit")
  @POST
  @Produces(Array(json))
  def limit(@DefaultValue("-1")@FormParam("place") p: Long) = placeDao.find(p) match {
    case None =>
      val exc = new NotFoundException(s"place:$p not found")
      mail ! SendError(exc)
      throw exc
      
    case Some(place) => userPlaceDao.find(EQCriterion("place", Key.create(classOf[Place], p)))(classOf[User])._1 match {
      case Nil =>
        val exc = new NotFoundException(s"place:$p admin could not be found")
        mail ! SendError(exc)
        throw exc
        
      case ups =>
        val root = new SimpleHash
        root.put("URL", s"$url/")
        root.put("images_url", s"$url/static/img/")

        val body = new MimeMultipart("alternative")
        body addBodyPart bindText(root, "eml_limit_txt.ftl")
        body addBodyPart bindHtml(root, "eml_limit_html.ftl")

        for (up <- ups)
          sendMail(support)(up.user.get.email.getEmail)(
            "You are close to your party limit on Waitlist Me", body)

        Response.ok.build
    }
  }
  
  /**
   * Send password reset email to the specified <code>User</code>.
   */
  @Path("reset")
  @POST
  @Produces(Array(json))
  def reset(
      @DefaultValue("-1")@FormParam("user") u: Long,
      @DefaultValue("")@FormParam("url") passwordUrl: String) = userDao.find(u) match {
    case None =>
      val exc = new NotFoundException(s"user:$u not found")
      mail ! SendError(exc)
      throw exc
      
    case Some(user) =>
      // check to see that reset token has been generated
      Option(user.password_reset_token) match {
        case None =>
          val exc = new NotFoundException(s"user:$u password token not found")
          mail ! SendError(exc)
          throw exc
          
        case Some(token) =>
          val root = new SimpleHash
          root.put("user", user.first_name)
          root.put("password_url", passwordUrl)
          root.put("URL", s"$url/")
          
          val body = new MimeMultipart("alternative")
          body addBodyPart bindText(root, "eml_reset_txt.ftl")
          body addBodyPart bindHtml(root, "eml_reset_html.ftl")
          
          sendMail(support)(new InternetAddress(user.email.getEmail, user.fullName))(
            "Waitlist Me password reset", body)
          
          Response.ok.build
      }
  }

  /**
   * Send the weekly stats report.
   */
  @Path("stats")
  @POST
  @Produces(Array(json))
  def stats = {
    val fut = memcache.get("last_weekly_active_places")
    val now = getNow
    val count = fut.get.asInstanceOf[Int]
    log.warning(s"count: $count")
    
    //log.warning(s"now: $now, future: $fut.get")
    
    Response.ok.build
    
//        now = datetime.datetime.now()
//    prior_week_finish = now - datetime.timedelta(days=7)
//    prior_week_start = now - datetime.timedelta(days=14)
//
//    # Aggregate the weekly stats in these temporary entities (DO NOT SAVE)
//    this_week_stats = DailyStats(date=now)
//    last_week_stats = DailyStats(date=now)
//    this_week_global_stats = GlobalStatsDaily(date=now)
//    last_week_global_stats = GlobalStatsDaily(date=now)
//
//    # stats for this week
//    stats = DailyStats.gql('WHERE place = :1 AND date >= :2 AND date < :3',
//                           None, prior_week_finish, now).fetch(20)
//    for stat in stats:
//      this_week_stats.increment(stat)
//
//    # stats for last week
//    stats = DailyStats.gql('WHERE place = :1 AND date >= :2 AND date < :3',
//                           None, prior_week_start, prior_week_finish).fetch(20)
//    for stat in stats:
//      last_week_stats.increment(stat)
//
//    # global stats for this week
//    global_stats = GlobalStatsDaily.gql('WHERE date >= :1 AND date < :2',
//                                        prior_week_finish, now).fetch(20)
//    for stat in global_stats:
//      this_week_global_stats.increment(stat)
//
//    global_stats = GlobalStatsDaily.gql('WHERE date >= :1 AND date < :2',
//                                        prior_week_start, prior_week_finish).fetch(20)
//    for stat in global_stats:
//      last_week_global_stats.increment(stat)
//
//    active_places = Place.gql('WHERE seated_last_week > :1', 0).count()
//
//    mkey = 'last_weekly_active_places'
//    last_week_active_places = memcache.get(mkey)
//    if not last_week_active_places:
//      last_week_active_places = 0
//    memcache.set(mkey, active_places)
//
//    email_data = {
//      'new_users': this_week_global_stats.new_users,
//      'diff_new_users': this_week_global_stats.new_users - last_week_global_stats.new_users,
//      'active_places': active_places,
//      'diff_active_places': active_places - last_week_active_places,
//      'sms_sent': this_week_stats.sms_sent,
//      'diff_sms_sent': this_week_stats.sms_sent - last_week_stats.sms_sent,
//      'total_seated': this_week_stats.total_seated,
//      'diff_total_seated': this_week_stats.total_seated - last_week_stats.total_seated,
//    }
//
//    subject = ( 'Waitlist Me: Weekly Stats (%s - %s)' %
//                (prior_week_finish.strftime('%D'), now.strftime('%D')) )
//    body = self.render_to_string('email/weekly_stats.txt', email_data)
//
//    logging.info('Sending Email: %s' % subject)
//    mail.send_mail(sender=constants.SUPPORT_EMAIL_ADDRESS, to=constants.NOSHLIST_STATS_EMAIL,
//                   subject=subject, body=body)
                   
  }
  
  /**
   * Send tip reminder email to the specified <code>User</code>.
   */
  @Path("tips")
  @POST
  @Produces(Array(json))
  def tips(@DefaultValue("-1")@FormParam("user") u: Long) = userDao.find(u) match {
    case None =>
      val exc = new NotFoundException(s"user:$u not found")
      mail ! SendError(exc)
      throw exc
      
    case Some(user) =>
      val root = new SimpleHash
      root.put("user", user.first_name)
      root.put("URL", s"$url/")
      root.put("images_url", s"$url/static/img/")

      val body = new MimeMultipart("alternative")
      body addBodyPart bindText(root, "eml_tips_txt.ftl")
      body addBodyPart bindHtml(root, "eml_tips_html.ftl")

      sendMail(support)(user.email.getEmail)(
        "Top Tips for Improving Business Results with Waitlist Me", body)

      Response.ok.build
  }

  /**
   * Send welcome email to the specified <code>User</code>.
   */
  @Path("welcome")
  @POST
  @Produces(Array(json))
  @Transact(TxnType.REQUIRED)
  def welcome(@DefaultValue("-1")@FormParam("user") u: Long) = userDao.find(u) match {
    case None =>
      val exc = new NotFoundException(s"user:$u not found")
      mail ! SendError(exc)
      throw exc
      
    case Some(user) =>
      val root = new SimpleHash
      root.put("user", user.first_name)
      root.put("URL", s"$url/")
      root.put("images_url", s"$url/static/img/")
      root.put("clover", Option(user.clover_id).isDefined)

      val body = new MimeMultipart("alternative")
      body addBodyPart bindText(root, "eml_welcome_txt.ftl")
      body addBodyPart bindHtml(root, "eml_welcome_html.ftl")

      sendMail(support)(user.email.getEmail)("Welcome to Waitlist Me!", body)

      // update user flags for welcome email
      if (!user.flags.contains(User.WELCOME_SENT)) {
        user.flags.add(User.WELCOME_SENT)
        userDao.persist(user)
      }

      Response.ok.build
  }
}