package me.waitlist.mail.rs
package traits

import javax.inject.Inject
import freemarker.template.Configuration
import freemarker.template.SimpleHash
import java.io.StringWriter
import javax.mail.internet.MimeBodyPart
import javax.activation.FileDataSource
import javax.activation.DataHandler
import javax.mail.Address
import javax.mail.Multipart
import javax.mail.internet.MimeMessage
import java.util.Properties
import javax.mail.Session
import javax.mail.internet.MimeMultipart
import javax.mail.Transport
import javax.mail.Message
import javax.ws.rs.core.Response
import javax.mail.internet.InternetAddress

/**
 * Trait for resources that require javamail functionality.
 * 
 * @author dsumera
 */
trait Mailable {
  
  // freemarker config
  @Inject private[this] var config: Configuration = _
  
  implicit def string2Email(email: String) = new InternetAddress(email)

  /**
   * Convenience method to bind root map to html template.
   */
  def bindHtml(model: SimpleHash, html: String) = {
      val out = new StringWriter
      config.getTemplate(html).process(model, out)
      val part = new MimeBodyPart
      part.setContent(out.toString, "text/html; charset=utf-8")
      part
  }
  
  /**
   * Convenience to bind root map to text template.
   */
  def bindText(model: SimpleHash, text: String) = {
      val out = new StringWriter
      config.getTemplate(text).process(model, out)
      val part = new MimeBodyPart
      part.setText(out.toString, "utf-8")
      part
  }
  
  /**
   * Convenience to create attachment.
   */
  def bindAttachment(file: String) = {
    val part = new MimeBodyPart
    part.setDataHandler(new DataHandler(new FileDataSource(file)))
    part.setFileName(file)
    part
  }
  
  /**
   * Convenience to send a mail. Returns an OK HTTP response if successful.
   */
  def sendMail(from: Address*)(to: Address*)(subject: String, body: Multipart, reply: Option[Address] = None) {
      val msg = new MimeMessage(Session.getDefaultInstance(new Properties, null))
      //msg.setHeader("Content-Type", "text/html; charset=UTF-8")
      msg.addFrom(from.toArray)
      msg.addRecipients(Message.RecipientType.TO, to.toArray)
      //msg.setRecipient(Message.RecipientType.TO, to)
      msg.setSubject(subject)
      if (reply.isDefined) msg.setReplyTo(Array(reply.get))
      // create message and send it
      //msg.setContent(multi, "text/html; charset=UTF-8")
      msg.setContent(body)
      Transport.send(msg)
  }
}