package me.waitlist.mail
package rs

import javax.inject.Singleton
import javax.ws.rs.Path

/**
 * Resource for task context path. All other subresource locators should go here.
 *
 * @author dsumera
 */
@Path("/task")
@Singleton
class TaskResource {

  @Path("send")
  def send = classOf[SendResource]
}