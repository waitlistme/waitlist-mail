<#noautoesc>

Dear, ${user?cap_first}!

Welcome to Waitlist Me!\n

You probably want to simplify the management of your waitlist and reservations while improving customer satisfaction. Waitlist Me lets you easily add people to an interactive waitlist and send texts and phone call notifications when you are ready for them. We have already helped all types of businesses serve over 100 million customers.
\n

So, what's the next step? Check out our Welcome Page to watch a short overview video and learn some tips on getting started and getting the most of Waitlist Me. Then download the app on an iPad, iPhone, or Android tablet or phone. You can also use the online waitlist from any browser.

Your Waitlist Me free account lets you use all the core waitlist and notification features for up to 100 customers a month. Waitlist Me Premium offers unlimited usage and other features like table management, reservations, downloadable reports, and customizable notifications. You can also get a free 14-day trial, and it’s only $19.99 after that.

If you have any questions or would like to send feedback, just shoot us an email at support@waitlist.me.

Cheers, 
The Waitlist Me team 

Want the latest news? Follow us on 
Facebook and Twitter
--

Get the iPad App!
http://itunes.apple.com/us/app/nosh/id442976546
</#noautoesc>
