<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> Your Waitlist Me Password! </title>
  </head><body>
  
<table width="100%" border="0" align="center" cellpadding="00" cellspacing="0">
  <tbody> 
  <tr> 
    <td bgcolor="#f6f6f6" style="padding-top:25px;"> 
      <table width="657" border="0" align="center" cellpadding="00" cellspacing="0">
        <tbody> 
        <tr> 
          <td height="500" align="center" valign="top" style="border-left:1px solid
                #a5a5a5; border-right:1px solid #a5a5a5; border-top:1px
                solid #a5a5a5;"> 
            <table width="657" border="0" align="center" cellpadding="00" cellspacing="0">
              <tbody> 
              <tr> 
              </tr>
              <tr> 
                <td colspan="2"> </td>
              </tr>
              <tr> 
                <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="padding-top:37px; padding-left:45px;"> 
                  <img src="${images_url}logo_email.png"/> </td>
              </tr>
              <tr> 
                <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:Arial;
                        font-size:14px; color:#535353; padding-left:50px;
                        padding-right:50px;
                        padding-top:22px; line-height:20px;"> 
                  <p><span style="font-size:18px; color:#2e2e2e"> Dear ${user?cap_first}, 
                    </span> <br />
                    <br />
                    Thank you for installing the Waitlist Me app on Clover.  Your user name is your email and here is the password we created for you:<br> ${password} <br />
                    <br />
                    You have a free month of Waitlist Me premium, which includes additional features for customizing text notifications, updating your table list, and running a number of reports on the Waitlist Me website. Be sure to check out our <a href="${URL}welcome">Welcome page</a> for an overview of getting started and more things you can do with Waitlist Me.<br />
<br>
You can also use the <a href="${URL}">www.waitlist.me</a> website to update your password, customize your settings, and even manage your waitlist from any browser.
<br />
                    <p><font face="Arial">Best regards, <br />
                    The Waitlist Me team <br />
                    </font> </p>
                </td>
              </tr>
              <tr> 
                <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="padding-left:45px; padding-top:15px;"> 
                </td>
              </tr>
              <tr> 
                <td colspan="2" bgcolor="#FFFFFF">&nbsp; </td>
              </tr>
              </tbody> 
            </table>
          </td>
        </tr>
        <tr> 
          <td> <img src="${images_url}bottom_line.jpg" width="661" height="11" /> </td>
        </tr>
        </tbody> 
      </table>
    </td>
  </tr>
  <tr> 
    <td bgcolor="#f6f6f6" style="padding-top:2px;"> 
      <table width="661" border="0" align="center" cellpadding="00" cellspacing="0">
        <tbody> 
        <tr> 
          <td align="center" valign="top" style="font-family:Arial, Helvetica,
                        sans-serif; font-weight:bold;
                        font-size:12px;
                        color:#625e5e;line-height:20px;"> 
            <p> Waitlist Me - The Simplest Waitlist App Ever<br />
              <a href="http://www.waitlist.me/terms_and_conditions" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-715">Terms 
              of use</a> <a href="http://www.waitlist.me/privacy_policy" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-717">Privacy 
              Policy</a> <a href="http://www.waitlist.me/press#contacts" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-719">Contacts</a> 
              <a href="http://www.waitlist.me/careers" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-721">Careers</a> 
              <a href="http://www.waitlist.me/about" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-723">About 
              Us</a> </p>
            <p style="color:#939393;"> <br />
            </p>
          </td>
        </tr>
        </tbody> 
      </table>
    </td>
  </tr>
  </tbody> 
</table>
        </body>
        </html>
