<#noautoesc>
<div>
    <img src="{{URL}}/static/img/logo_email.png" alt="NoshList"/>
</div><br/>Dear ${user?cap_first},
<br/>
<br/>To reset your Waitlist Me password, click the link below or paste it into your browser. That will take you to a web page where you can create a new password.
<br/>
<br/><a href="${password_url}">${password_url}</a>
<br/>
<br/>If you remember your password or if you did not initiate this request, just disregard this e-mail and login. To protect your account, the link will expire in three days.
<br/>
<br/>Sincerely,
<br/>The Waitlist Me Team
</#noautoesc>
