<#noautoesc>

Dear ${first?cap_first},

You have a few days left in your free Waitlist Me ${tier} free trial, and we hope you are enjoying the service.  If you are, you don’t need to do anything and you will be charged monthly for ${amount} starting on ${billing?date!"n/a"}.

If you choose not to continue with Waitlist Me ${tier} you can just login to www.waitlist.me and downgrade the service in your Account area, if you haven’t done so already, and you won’t be charged for the upcoming month. After that you will still be able to use the Waitlist Me Basic features for up to 100 customers a month.

Clover users can change subscriptions in the Clover App Market.

Best regards,
The Waitlist Me Team

</#noautoesc>
