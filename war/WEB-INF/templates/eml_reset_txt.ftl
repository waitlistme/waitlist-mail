<#noautoesc>

Dear ${user?cap_first},

To reset your Waitlist Me password, click the link below or paste it into your browser. That will take you to a web page where you can create a new password.

${password_url}

If you remember your password or if you did not initiate this request, just disregard this e-mail and login. To protect your account, the link will expire in three days.

Sincerely,
The Waitlist Me Team
</#noautoesc>
