<#noautoesc>

Business Name: ${business_name?cap_first} 
<#if num_accounts gt 0>
Number of Accounts: ${num_accounts} 
</#if>
Email: ${email}
<#if phone != "">
Phone Number: ${phone} 
</#if>
Questions or Comments: ${comments?cap_first}                   

</#noautoesc>
