<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> Welcome to Waitlist Me! </title>
  </head><body>
  
<table width="100%" border="0" align="center" cellpadding="00" cellspacing="0">
  <tbody> 
  <tr> 
    <td bgcolor="#f6f6f6" style="padding-top:25px;"> 
      <table width="657" border="0" align="center" cellpadding="00" cellspacing="0">
        <tbody> 
        <tr> 
          <td height="500" align="center" valign="top" style="border-left:1px solid
                #a5a5a5; border-right:1px solid #a5a5a5; border-top:1px
                solid #a5a5a5;"> 
            <table width="657" border="0" align="center" cellpadding="00" cellspacing="0">
              <tbody> 
              <tr> 
                
              </tr>
              <tr> 
                <td colspan="2"> </td>
              </tr>
              <tr> 
                <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="padding-top:37px; padding-left:45px;"> 
                  <img src="${images_url}logo_email.png"/> </td>
              </tr>
              <tr> 
                <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:Arial;
                        font-size:14px; color:#535353; padding-left:50px;
                        padding-right:50px;
                        padding-top:22px; line-height:20px;"> 
                  <p><span style="font-size:18px; color:#2e2e2e"> Dear ${user?cap_first}, 
                    </span> <br />
                    <br />
                    <font face="Arial">Welcome to Waitlist Me!
                    <br />
                    <br />
                    If you are looking to simplify the management of your waitlist and reservations while improving customer satisfaction and other business results, then you are in good company. Waitlist Me has been used by all types of businesses to serve over 100 million customers.<br />
                    <br />
                    So, what's the next step? Check out our <a style="text-decoration:none; color:#3eb0d7;" href="${URL}<#if clover>welcome-clover<#else>welcome</#if>">Welcome Page</a> to watch a short <a href="http://youtu.be/YmHd-LK2D3A" style="text-decoration:none; color:#3eb0d7;">overview video</a> and learn some tips on getting started and getting the most of Waitlist Me. Then download the app on an <a href="https://itunes.apple.com/us/app/noshlist/id573230074?mt=8" target="_blank" style="text-decoration:none; color:#3eb0d7;">iPad</a>, iPhone, or<a href="https://play.google.com/store/apps/details?id=com.nosh.list.NoshList" target="_blank" style="text-decoration:none; color:#3eb0d7;"> Android</a> tablet or phone. You can also use the <a href="${URL}waitlist" target="_blank" style="text-decoration:none; color:#3eb0d7;">online waitlist</a> from any browser.
                  <p>
                    <font face="Arial">
The Waitlist Me basic account lets you use core waitlist and notification features free for up to 100 customers a month. <a target="_blank" style="text-decoration:none; color:#3eb0d7;" href="${URL}premium">Waitlist Me Premium</a> offers unlimited usage and other features like table management, reservations, downloadable reports, and customizable notifications. You can also get a free 14-day trial, and it’s only $19.99 after that.
                    </font>
                  </p>
                  <p>
                  For even more, check out <a href="${URL}account" style="text-decoration:none; color:#3eb0d7;"> Waitlist Me Pro</a> for additional resource management and customer survey features.
                  </p>
                  <p>
                    If you have any questions or would like to send feedback, just shoot us an email at <a href="mailto:support@waitlist.me" style="text-decoration:none; color:#3eb0d7;">support@waitlist.me</a>.
                  </p>                   


                  <p><font face="Arial">Cheers, <br />
                    The Waitlist Me team <br />
                    <br />
                    Want the latest news? Follow us on <br />
                    <a href="http://www.facebook.com/WaitlistMe" target="_blank" style="text-decoration:none; color:#3eb0d7;">Facebook</a> 
                    and <a href="http://www.twitter.com/waitlistme" target="_blank" style="text-decoration:none; color:#3eb0d7;">Twitter</a> 
                    </font> </p>
                </td>
              </tr>
              <tr> 
                <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="padding-left:45px; padding-top:15px;"> 
                </td>
              </tr>
              <tr> 
                <td colspan="2" bgcolor="#FFFFFF">&nbsp; </td>
              </tr>
              </tbody> 
            </table>
          </td>
        </tr>
        <tr> 
          <td> <img src="${images_url}bottom_line.jpg" width="661" height="11" /> </td>
        </tr>
        </tbody> 
      </table>
    </td>
  </tr>
  <tr> 
    <td bgcolor="#f6f6f6" style="padding-top:2px;"> 
      <table width="661" border="0" align="center" cellpadding="00" cellspacing="0">
        <tbody> 
        <tr> 
          <td align="center" valign="top" style="font-family:Arial, Helvetica,
                        sans-serif; font-weight:bold;
                        font-size:12px;
                        color:#625e5e;line-height:20px;"> 
            <p> Waitlist Me - The Simplest Waitlist App Ever<br />
              <a href="${URL}terms_and_conditions" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-715">Terms 
              of use</a> <a href="${URL}privacy_policy" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-717">Privacy 
              Policy</a> <a href="${URL}contacts" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-719">Contact</a> 
              <a href="${URL}about" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-723">About 
              Us</a> </p>
            <p style="color:#939393;"> <br />
            </p>
          </td>
        </tr>
        </tbody> 
      </table>
    </td>
  </tr>
  </tbody> 
</table>
        </body>
        </html>
