<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>7 day Reminder email! </title>
  </head><body>
  
<table width="100%" border="0" align="center" cellpadding="00" cellspacing="0">
  <tbody> 
  <tr> 
    <td bgcolor="#f6f6f6" style="padding-top:25px;"> 
      <table width="657" border="0" align="center" cellpadding="00" cellspacing="0">
        <tbody> 
        <tr> 
          <td height="500" align="center" valign="top" style="border-left:1px solid
                #a5a5a5; border-right:1px solid #a5a5a5; border-top:1px
                solid #a5a5a5;"> 
            <table width="657" border="0" align="center" cellpadding="00" cellspacing="0">
              <tbody> 
              <tr> 
              </tr>
              <tr> 
                <td colspan="2"> </td>
              </tr>
              <tr> 
                <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="padding-top:37px; padding-left:45px;"> 
                  <img src="${images_url}logo_email.png"/> </td>
              </tr>
              <tr> 
                <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:Arial;
                        font-size:14px; color:#535353; padding-left:50px;
                        padding-right:50px;
                        padding-top:22px; line-height:20px;"> 
                  <p><span style="font-size:18px; color:#2e2e2e"> Hi ${user?cap_first}, 
                    </span> <br />
                    <br />
                    <font face="Arial">We hope you are enjoying using Waitlist Me!
                    <br />
                    <br />
                    Now that you have had some time to check out the basics, here are some <a href="http://www.waitlist.me/tips">top tips</a> on how Waitlist Me can help you improve your business results.<br />
                    <br />
                    Take a few minutes to see some ways Waitlist Me has been proven to retain more customers, boost customer satisfaction, increase profitability, and more.  You’ll be glad you did.
                    <p><font face="Arial">Best regards,<br />
                    The Waitlist Me team<br />
                    <br />
                    Want the latest news? Follow us on <br />
                    <a href="http://www.facebook.com/WaitlistMe" target="_blank" style="text-decoration:none; color:#3eb0d7;">Facebook</a> 
                    and <a href="http://www.twitter.com/waitlistme" target="_blank" style="text-decoration:none; color:#3eb0d7;">Twitter</a> 
                    </font> </p>
                </td>
              </tr>
              <tr> 
                <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="padding-left:45px; padding-top:15px;"> 
                </td>
              </tr>
              <tr> 
                <td colspan="2" bgcolor="#FFFFFF">&nbsp; </td>
              </tr>
              </tbody> 
            </table>
          </td>
        </tr>
        <tr> 
          <td> <img src="${images_url}bottom_line.jpg" width="661" height="11" /> </td>
        </tr>
        </tbody> 
      </table>
    </td>
  </tr>
  <tr> 
    <td bgcolor="#f6f6f6" style="padding-top:2px;"> 
      <table width="661" border="0" align="center" cellpadding="00" cellspacing="0">
        <tbody> 
        <tr> 
          <td align="center" valign="top" style="font-family:Arial, Helvetica,
                        sans-serif; font-weight:bold;
                        font-size:12px;
                        color:#625e5e;line-height:20px;"> 
            <p> Waitlist Me - The Simplest Waitlist App Ever<br />
              <a href="${URL}terms_and_conditions" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-715">Terms 
              of use</a> <a href="${URL}privacy_policy" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-717">Privacy 
              Policy</a> <a href="${URL}contacts" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-719">Contact</a> 
              <a href="${URL}about" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-723">About 
              Us</a> </p>
            <p style="color:#939393;"> <br />
            </p>
          </td>
        </tr>
        </tbody> 
      </table>
    </td>
  </tr>
  </tbody> 
</table>
        </body>
        </html>
