<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> Welcome to Waitlist Me! </title>
  </head><body>
  
<table width="100%" border="0" align="center" cellpadding="00" cellspacing="0">
  <tbody> 
  <tr> 
    <td bgcolor="#f6f6f6" style="padding-top:25px;"> 
      <table width="657" border="0" align="center" cellpadding="00" cellspacing="0">
        <tbody> 
        <tr> 
          <td height="500" align="center" valign="top" style="border-left:1px solid
                #a5a5a5; border-right:1px solid #a5a5a5; border-top:1px
                solid #a5a5a5;"> 
            <table width="657" border="0" align="center" cellpadding="00" cellspacing="0">
              <tbody> 
              <tr> 
              </tr>
              <tr> 
                <td colspan="2"> </td>
              </tr>
              <tr> 
                <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="padding-top:37px; padding-left:45px;"> 
                 <img src="${images_url}logo_email.png"/></td>
              </tr>
              <tr> 
                <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:Arial;
                        font-size:14px; color:#535353; padding-left:50px;
                        padding-right:50px;
                        padding-top:22px; line-height:20px;"> 
                  <p><span style="font-size:18px; color:#2e2e2e"> Dear ${first?cap_first}, 
                    </span> <br />
                    <br />
                    <font face="Arial">
                    You have a few days left in your free Waitlist Me ${tier} free trial, and we hope you are enjoying the service.  If you are, you don’t need to do anything and you will be charged monthly for ${amount} starting on ${billing?date!"n/a"}.<br/>
                    </font>
                  </p>
                  <font face="Arial">
                  <p>
                    If you choose not to continue with Waitlist Me ${tier} you can just login to www.waitlist.me and downgrade the service in your Account area, if you haven’t done so already, and you won’t be charged for the upcoming month. After that you will still be able to use the Waitlist Me Basic features for up to 100 customers a month.     
                  </font>
                  </p>
                  <p>
                  <font face="Arial">
                    Clover users can change subscriptions in the Clover App Market.
                  </font>
                  </p>
                  <p><font face="Arial">Best regards,<br />
                    The Waitlist Me Team <br />
                    </font></p>
                </td>
              </tr>
              <tr> 
                <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF" style="padding-left:45px; padding-top:15px;"> 
                </td>
              </tr>
              <tr> 
                <td colspan="2" bgcolor="#FFFFFF">&nbsp; </td>
              </tr>
              </tbody> 
            </table>
          </td>
        </tr>
        <tr> 
          <td> <img src="${images_url}bottom_line.jpg" width="661" height="11" /> </td>
        </tr>
        </tbody> 
      </table>
    </td>
  </tr>
  <tr> 
    <td bgcolor="#f6f6f6" style="padding-top:2px;"> 
      <table width="661" border="0" align="center" cellpadding="00" cellspacing="0">
        <tbody> 
        <tr> 
          <td align="center" valign="top" style="font-family:Arial, Helvetica,
                        sans-serif; font-weight:bold;
                        font-size:12px;
                        color:#625e5e;line-height:20px;"> 
            <p> Waitlist Me - The Simplest Waitlist App Ever<br />
              <a href="http://www.waitlist.me/terms_and_conditions" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-715">Terms 
              of use</a> <a href="http://www.waitlist.me/privacy_policy" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-717">Privacy 
              Policy</a> <a href="http://www.waitlist.me/press#contacts" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-719">Contacts</a> 
              <a href="http://www.waitlist.me/careers" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-721">Careers</a> 
              <a href="http://www.waitlist.me/about" target="_blank" style="text-decoration:none; color:#625E5E; margin-right:10px;" class="inf-track-723">About 
              Us</a> </p>
            <p style="color:#939393;"> <br />
            </p>
          </td>
        </tr>
        </tbody> 
      </table>
    </td>
  </tr>
  </tbody> 
</table>
        </body>
        </html>
