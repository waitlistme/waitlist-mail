Hi ${user?cap_first},
 
We hope you are enjoying using Waitlist Me!
 
Now that you have had some time to check out the basics, here are some top tips on how Waitlist Me can help you improve your business results. 
 
Take a few minutes to see some ways Waitlist Me has been proven to retain more customers, boost customer satisfaction, increase profitability, and more.  You’ll be glad you did.
 
Best regards,
The Waitlist Me team