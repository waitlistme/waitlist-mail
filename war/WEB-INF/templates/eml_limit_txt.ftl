<#noautoesc>
Hi, 
                
Thanks for using Waitlist Me!        

This is courtesy notification that you have added 80 parties this month and are approaching your monthly limit of 100 parties. If the app is working well for you, please consider upgrading for unlimited usage and all the features.

Best regards,
The Waitlist Me Team 
</#noautoesc>
