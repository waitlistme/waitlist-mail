<#noautoesc>
Dear ${user?cap_first},

Thank you for installing the Waitlist Me app on Clover.  Your user name is your email and here is the password we created for you: ${password}

You have a free month of Waitlist Me premium, which includes additional features for customizing text notifications, updating your table list, and running a number of reports on the Waitlist Me website. Be sure to check out our Welcome page for an overview of getting started and more things you can do with Waitlist Me.

You can also use the www.waitlist.me site to update your password as well as see an online view of your waitlist and manage your account settings.


Best regards,
The Waitlist Me team
</#noautoesc>
